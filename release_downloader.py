import requests
import os
import shutil

def main():
    """
    download all requests
    """
    print('Preparing to download releases...')
    headers = {
        "PRIVATE-TOKEN": os.getenv('PRIVATE_TOKEN')
    }
    releases = requests.get(f'https://gitlab.com/api/v4/projects/{os.getenv("CI_PROJECT_ID")}/releases', headers=headers)
    print('- Found', len(releases.json()))

    if not os.path.exists('public'):
        os.makedirs("public")

    a_hrefs = ""
    for release in releases.json():
        try:
            tag = release.get('tag_name', {})
        except AttributeError as e:
            print(releases.json())
            raise e
        commit_sha = release.get('commit', {}).get('id')
        name = release.get('name')
        description = release.get('description')
        local_filename = f'{tag}.zip'
        sub_path = None

        print(f'  - {tag}')

        a_hrefs += f'<a id="{tag}" href="{local_filename}" download>{name}</a>: {description}<br>'

        download_template = f'''
            <!DOCTYPE html>
            <html>
            <head>
            </head>
            <body>
            <a id="{tag}" href="{local_filename}" download>{name}</a>: {description}<br>
            <script>
                var hash = window.location.pathname.substr(window.location.pathname.lastIndexOf('/') + 1).slice(0,-5);
                if (hash != null && hash != "") {{
                    document.getElementById(hash).click();
                    history.back();
                }}
            </script>
            </body>
            </html>
        '''
        
        # the intent is to always re-write the HTML files.
        # I was using Anchor Tags (#) instead of indvidual files
        # but GitLab's login redirect strips everything after the
        # `#`
        with open(f'public/{tag}.html', 'w') as f:
            f.write(download_template)
        
        if os.path.isfile(f'public/{tag}.zip'):
            continue
        
        url = f"https://gitlab.com/api/v4/projects/38454884/repository/archive?sha={commit_sha}"
        if sub_path:
            url += "&path=<path>"
        with requests.get(url, stream=True, headers=headers) as r:
            with open(f'public/{local_filename}', 'wb') as f:
                shutil.copyfileobj(r.raw, f)
                print('Saved file:', f'public/{local_filename}')

    template = f'''
        <!DOCTYPE html>
        <html>
        <head>
        </head>
        <body>

        <h1>
            Releases for 
            <a href="{os.getenv("CI_PROJECT_URL")}">{os.getenv("CI_PROJECT_TITLE")}
            </a>
        </h1>
        <div>
        {a_hrefs}
        </div>
        <script>
            var hash = window.location.hash.substr(1);
            if (hash != null && hash != "") {{
                document.getElementById(hash).click();
                history.back();
            }}
        </script>
        </body>
        </html>
    '''

    with open('public/index.html', 'w') as f:
        f.write(template)



if __name__ == '__main__':
    main()

